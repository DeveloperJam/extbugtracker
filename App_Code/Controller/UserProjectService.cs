﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class UserProjectService : System.Web.Services.WebService {
private string _connectionstring = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public void addUserToProject(int userID, int projectID)
    {
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spAddUserToProject", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserID", userID);
                cmd.Parameters.AddWithValue("@ProjectID", projectID);
                _connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<Project> getUserProjects(int userID)
    {
        List<Project> project = new List<Project>();
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spGetUserProjects", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserID", userID);
                _connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        project.Add(new Project((int)reader[0], (string)reader[1]));
                    reader.Close();
                }
            }
        }
        return project;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<ViewUser> getProjectMembers(int projectID)
    {
        List<ViewUser> memberList = new List<ViewUser>();
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spGetProjectMembers", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProjectID", projectID);
                _connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        memberList.Add(new ViewUser((int)reader[0], (string)reader[1]));
                    reader.Close();
                }
            }
        }
        return memberList;
    }

}
