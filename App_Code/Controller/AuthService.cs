﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using details;
using System.Configuration;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Data;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AuthService : System.Web.Services.WebService
{
    string _connectionstring = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public Nullable<bool> loginUser(String userName, String password)
    {    
        List<User> users = new  List<User>();
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spGetLoginUser", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", userName);
                cmd.Parameters.AddWithValue("@Password", password);
                _connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        users.Add(new User((string)reader[0], (string)reader[1]));
                    reader.Close();
                }  
            }
        }
        //BadBAD METHOD
        if (users.Count > 0)
        {
            return true;      
        }
        else
        {
            return false;          
        }      
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
    public void createUser(String userName, String password)
    {
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spCreateUser", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", userName);
                cmd.Parameters.AddWithValue("@Password", password);
                _connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<ViewUser> getUserFriends()
    {
        List<ViewUser> users = new List<ViewUser>();
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spGetUserFriends", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                _connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        users.Add(new ViewUser((int)reader[0], (string)reader[1]));
                    reader.Close();
                }  
            }
        }
        return users;
    }


}
