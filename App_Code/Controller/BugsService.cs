﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using details;
using System.Configuration;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Data;
using System.Xml.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class BugsService : System.Web.Services.WebService
{
    string _connectionstring = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<Bug> getProjectBugs(int projectID)
    {
        List<Bug> bugs = new List<Bug>();
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spGetProjectBugs", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProjectID", projectID);
                _connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        bugs.Add(new Bug((int)reader["BugID"], (string)reader["Description"], (string)reader["Type"], (string)reader["Status"], (string)reader["Priority"]));
                    reader.Close();
                }
            }
        } 
        return bugs;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public void createBug(string description, string type, string status, string priority, int projectID, int assignedTo, string createdBy)
    {
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spCreateBug", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Description", description);
                cmd.Parameters.AddWithValue("@Type", type);
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@Priority", priority);
                cmd.Parameters.AddWithValue("@ProjectID", projectID);
                cmd.Parameters.AddWithValue("@AssignedTo", assignedTo);
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
               
                _connection.Open();
                cmd.ExecuteNonQuery();
            }
        } 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public List<Bug> searchBugs(int userID, string searchStr)
    {
        List<Bug> bugs = new List<Bug>();
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spSearchBugs", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserID", userID);
                cmd.Parameters.AddWithValue("@SearchStr", searchStr);
                _connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        //IS THIS PROPER?
                        bugs.Add(new Bug((int)reader["BugID"], (string)reader["Description"], (string)reader["Type"], (string)reader["Status"], (string)reader["Priority"]));
                    reader.Close();
                }
            }
        }
        return bugs;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public void updateBugs(Bug updatedBug)
    {
        XmlSerializer ser = new XmlSerializer(updatedBug.GetType());
        Console.WriteLine(ser);
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spUpdateBug", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UpdatedBug", updatedBug);
                _connection.Open();
                cmd.ExecuteNonQuery();
            }
        } 
    }
}
