﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ProjectService : System.Web.Services.WebService {
private string _connectionstring = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true, XmlSerializeString = false)]
    public void createProject(int userID, string projectName)
    {
        using (SqlConnection _connection = new SqlConnection(_connectionstring))
        {
            using (SqlCommand cmd = new SqlCommand("spCreateProject", _connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserID", userID);
                cmd.Parameters.AddWithValue("@ProjectName", projectName);
                _connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}


