﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Project
/// </summary>
public class Project
{
    public int projectID { get; set; }
    public string projectName { get; set; }

	public Project()
	{
      
	}
    public Project(int projectID, string projectName)
        {
            this.projectID = projectID;
            this.projectName = projectName;           
        }
}

