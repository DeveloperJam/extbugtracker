﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Bug
	{
		public int bugID { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public string status { get; set; }
        public string priority { get; set; }
        public Nullable<int> assignedTo { get; set; }
        public Nullable<int> createdBy { get; set; }
        public string startDate{ get; set; }

        public Bug()
        {
        }
        public Bug(int bugID, string description, string type, string status, string priority)
        {
            this.bugID = bugID;
            this.description = description;
            this.type = type;
            this.status = status;
            this.priority = priority;
            this.assignedTo = 1;
            this.createdBy = 1;
            this.startDate = DateTime.Today.ToString("dd/MM/yyyy");
        }

}
