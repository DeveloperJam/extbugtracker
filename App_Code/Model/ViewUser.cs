﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ViewUser
/// </summary>
public class ViewUser
{
    public int userID{ get; set; }
    public string userName { get; set; }

	public ViewUser()
	{
	
	}

    public ViewUser(int userID, string userName)
    {
        this.userID = userID;
        this.userName = userName;

    }
}

