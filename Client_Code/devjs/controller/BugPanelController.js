/*
BugPanelController
*/
Ext.define('extjs.controller.BugPanelController',{
extend: 'Ext.app.Controller',
requires:['extjs.view.CreationPanel'],
init:function(){
  this.control({
      '#btnCreate':{
        click:this.loadBugWindow
      },
      '#btnCrProj':{
        click:this.loadProjectWindow
      },
      '#invBut':{
        click:this.loadInvitation
      }
    });
    },
    loadBugWindow:function(button, event, eOpts){
  
     Ext.create('extjs.view.bug.BugWindow').show();
    } ,  
    loadProjectWindow:function(button, event, eOpts){
     Ext.create('extjs.view.project.ProjectWindow').show();
    },  
    loadInvitation:function(button, event, eOpts){
     Ext.create('extjs.view.inv.InvWindow').show();
    }
});




