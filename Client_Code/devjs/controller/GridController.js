/*
GridController
*/
Ext.define('extjs.controller.GridController',{
extend: 'Ext.app.Controller',
requires:['extjs.view.bug.BugGridPanel','extjs.view.bug.UpdateWindow','extjs.view.bug.UpdatePanel'],
init:function(){
  this.control({
      'projectcombo':{
        select:this.loadCombo
      },
        'buggridpanel':{
        itemdblclick:this.loadUpdate
      },
         '#updateSubmit':{
        click:this.submitUpdate
      }
    });
    },
    loadCombo:function(button, event, eOpts){
     Ext.data.StoreManager.lookup('gridStore').proxy.extraParams = {projectID: JSON.stringify(button.value)};
     Ext.data.StoreManager.lookup('gridStore').load();
     Ext.data.StoreManager.lookup('memberStore').proxy.extraParams = {projectID : JSON.stringify(button.value)};
     Ext.data.StoreManager.lookup('memberStore').load();
    },
    loadUpdate:function(buggrid,record){
       Ext.create('extjs.view.bug.UpdateWindow').show().loadRecord(record);

    },
    submitUpdate:function(button, event, eOpts){
        var win = button.up('window'),
        form = win.down('form');
       //var form = this.down('BugWindow').getForm();
            form.submit({
              success: function(form, action) {
               Ext.Msg.alert("Success","Bug Successfully Updated");
               Ext.data.StoreManager.lookup('gridStore').load();
               win.close();
             },
             failure: function(form, action) {
              Ext.Msg.alert('Failed', action.result.msg);
            }
          });
          
    }
});



