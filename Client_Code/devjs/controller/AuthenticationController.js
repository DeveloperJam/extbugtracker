/*
AuthenticationController
*/
Ext.define('extjs.controller.AuthenticationController',{
  extend: 'Ext.app.Controller',
  requires:['extjs.view.Viewport','extjs.view.Workspace','extjs.view.authentication.Login'],
  refs : [{
    ref: 'viewportView',
    selector: 'viewport'
  }, {
    ref: 'workspaceView',
    selector: 'workspace'
  }],
  init:function(){
    this.control({

      '#btnLogin':{
        click:this.onButtonLogin
      },
      '#regSubmit':{
        click:this.onButtonRegister
      }
    }); 

  },
  onButtonLogin:function(button, event, eOpts){
    var viewport = this.getViewportView();
    extjs.user = ("user",'3');
    viewport.removeAll(true);
    Ext.create('extjs.view.Workspace');
  }, 
  onButtonRegister:function(button, event, eOpts){
    var form = this.up('form').getForm();
    form.submit({
      success: function(form, action) {
       Ext.Msg.alert("Success","User Successfully Added, Please login to continue");

     },
     failure: function(form, action) {
      Ext.Msg.alert('Failed', action.result.msg);
    }
  });

  }

});



