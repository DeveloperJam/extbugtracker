/*
ProjectComboController
*/
Ext.define('extjs.controller.ProjectComboController',{
extend: 'Ext.app.Controller',
requires:['extjs.view.bug.BugWindow'],
init:function(){

  this.control({
      'projectcombo':{
        select:this.loadCombo
      }
    });
    },
    loadCombo:function(button, event, eOpts){
     Ext.data.StoreManager.lookup('gridStore').proxy.extraParams = {projectID: JSON.stringify(button.value)};
     Ext.data.StoreManager.lookup('gridStore').load();
     Ext.data.StoreManager.lookup('memberStore').proxy.extraParams = {projectID : JSON.stringify(button.value)};
     Ext.data.StoreManager.lookup('memberStore').load();
    }
});



