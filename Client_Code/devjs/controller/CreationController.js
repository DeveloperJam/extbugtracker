/*
CreationController
*/
Ext.define('extjs.controller.CreationController',{
extend: 'Ext.app.Controller',
init:function(){
  this.control({
      '#bugSubmit':{
        click:this.submitBugCreate
      },
      '#projSubmit':{
        click:this.submitProjectCreate
      },
      '#invSubmit':{
        click:this.submitInvCreate
      },
    });
    },
     submitBugCreate:function(button, event, eOpts){
       var win = button.up('window'),
        form = win.down('form');
       //var form = this.down('BugWindow').getForm();
            form.submit({
              success: function(form, action) {
               Ext.Msg.alert("Success","Bug Successfully Added");
               Ext.data.StoreManager.lookup('gridStore').load();
               win.close();
             },
             failure: function(form, action) {
              Ext.Msg.alert('Failed', action.result.msg);
            }
          });
          
    },
     submitProjectCreate:function(button, event, eOpts){
        var win = button.up('window'),
        form = win.down('form');
          if (form.isValid()) {
            form.submit({
              success: function(form, action) {
               Ext.Msg.alert("Success","Project Successfully Added");
                Ext.data.StoreManager.lookup('gridStore').load();
                win.close();
             },
             failure: function(form, action) {
            //  Ext.Msg.alert('Failed', action.result.msg);
            }
          });
          }
    }, 
    submitInvCreate:function(button, event, eOpts){
      var win = button.up('window'),
        form = win.down('form');
          if (form.isValid()) {
            form.submit({
              success: function(form, action) {
               Ext.Msg.alert("Success","Invitation Successfully Added");
                Ext.data.StoreManager.lookup('gridStore').load();
                win.close();
             },
             failure: function(form, action) {
            //  Ext.Msg.alert('Failed', action.result.msg);
            }
          });
          }   
    }
});

//