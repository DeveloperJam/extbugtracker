

Ext.Loader.setConfig({enabled:true});
Ext.application({
    name: 'extjs',
    appFolder: 'scripts/devjs',
    models: ['BugWindow','BugGrid','ProjectCombo'],
    controllers: ['BugPanelController','ProjectComboController','AuthenticationController','CreationController','GridController'],
    launch: function() {
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
    if(extjs.user != null){
        extjs.user = Ext.state.Manager.get('user');
        Ext.create('extjs.view.Viewport');
    }else{
        Ext.create('extjs.view.Workspace');   
        extjs.user  = ('3')
   }   
}
});


