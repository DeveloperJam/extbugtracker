
/*
Project Window for Project Panel
*/
Ext.define('extjs.view.project.ProjectWindow', {
	extend: 'Ext.window.Window',
	alias:'widget.projectwindow',  
	width:600,
	height:100,
	maximizable:false,
	layout:'fit',
	title:"Create Bug",
	items: new Ext.create('extjs.view.project.ProjectPanel')
});

