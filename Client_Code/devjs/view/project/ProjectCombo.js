/*
ProjectCombo
*/
Ext.define('extjs.view.project.ProjectCombo',{
  extend:'Ext.form.ComboBox',
  alias:'widget.projectcombo',
  forceSelection: true,
  emptyText: 'All...',
  valueField:'projectID',
  fieldLabel: 'Currently Selected Project',
  name: 'project',
  store: Ext.create('extjs.store.DropProject'),
  queryMode:'local',
  displayField:'projectName',
});



