
/*
Project Panel for Project create
*/
Ext.define('extjs.view.project.ProjectPanel', {
  extend: 'Ext.form.Panel',
  alias:'widget.projectpanel',
  require:['extjs.store.DropFriends'],
  bodyPadding: 5,
  url: 'http://localhost:9000/ProjectService.asmx/createProject',
  baseParams: {UserID : extjs.user},
  defaultType: 'textfield',
  defaults: {
    width: 500
  },
  items: [{
    fieldLabel: 'ProjectName',
    name: 'projectName',
    allowBlank: false,
    emptyText: 'Enter a New Project Name...'
  }
 ],
  buttons: [
  {
    text: 'Submit',
        formBind: true,
         itemId: 'projSubmit',
      }],
    });

