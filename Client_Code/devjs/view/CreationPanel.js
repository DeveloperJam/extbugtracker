/*
Triggers create bug model window
*/
Ext.define('extjs.view.CreationPanel', {
	extend:'Ext.panel.Panel',
	alias:'widget.createbug',
	items:[{
		xtype:'button',
		text:'Create Bug',
		itemId: 'btnCreate',
	},{
		xtype:'button',
		text:'Create Project',
		itemId: 'btnCrProj',
	},{
		xtype:'button',
		text:'Invite To Project',
		itemId: 'invBut',
	}]

});

