
/*
Project update window
*/
Ext.define('extjs.view.bug.UpdateWindow', {
  extend: 'Ext.window.Window',
  alias:'widget.updatewindow',  
  maximizable:false,
  layout:'fit',
  title:"Update Issue",
  items: Ext.create('extjs.view.bug.UpdatePanel'),

  loadRecord: function(rec) {
    Ext.getCmp('bugIDID').setValue(rec.data.bugID);
    Ext.getCmp('descriptionID').setValue(rec.data.description);
    Ext.getCmp('bugTypeID').setValue(rec.data.type);
    Ext.getCmp('bugStatusID').setValue(rec.data.status);
    Ext.getCmp('bugPriorityID').setValue(rec.data.priority);
    Ext.getCmp('assignedToID').setValue(rec.data.assignedTo);

                      // simplified - just update the html. May be replaced with a dataview         
                    }
                  });

