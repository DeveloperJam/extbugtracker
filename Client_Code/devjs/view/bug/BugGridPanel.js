/*
Bug grid loaded on workspace
Need to abstract the store
*/
var gridStore = new Ext.data.ArrayStore({
  autoLoad: true,  
  storeId: 'gridStore',
                  // model: 'extjs.model.BugGrid',
                  proxy: new Ext.data.HttpProxy({
                    type: 'ajax',
                    method:'GET',
                    url: 'http://localhost:9000/BugsService.asmx/getProjectBugs', 
                    extraParams: {projectID: 1},
                    headers: {'Content-Type' : 'application/json'},                              
                    reader: {
                      type: 'json',                                           
                      root: 'd'
                    },    
                  }),
                  fields: ['bugID', 'description','type','status','priority','assignedTo','createdBy','startDate'] 
                });

Ext.define('extjs.view.bug.BugGridPanel', {
  extend: 'Ext.grid.GridPanel',
  alias: 'widget.buggridpanel',
  title: 'Currently Active Bugs (Click a Record To Update)',
                 // selType: 'cellmodel',
                 border: true,
                        store: gridStore, //Ext.data.StoreManager.lookup('myStore'),          use only if multiple ui are sharing a store 
                        columns: [
                        { header: 'BugID', dataIndex: 'bugID', sortable: true , flex: 1, sortable: false }, 
                        { header: 'Description', dataIndex: 'description', flex: 1, sortable: false },
                        { header: 'Type', dataIndex: 'type', sortable: true,   flex: 1  }, 
                        { header: 'Status', dataIndex: 'status', sortable: true },
                        { header: 'Priority', dataIndex: 'priority', sortable: true,   flex: 1  },
                        { header: 'AssignedTo', dataIndex: 'assignedTo', sortable: true },
                        { header: 'CreatedBy', dataIndex: 'createdBy', sortable: true  },
                        { header: 'CreatedDate', dataIndex: 'startDate', sortable: true  },   
                        { header: 'EndDate', dataIndex: '', sortable: true  },
                        { header: 'Project', dataIndex: '', sortable: true  },                                       
                        ],
                      });
