
/*
Search Panel --TODO
*/
Ext.define('extjs.view.bug.UpdatePanel', {
  extend: 'Ext.form.Panel',
  alias: 'widget.updatepanel',
  url: 'http://localhost:9000/BugsService.asmx/updateBugs',
  width: 400,
  height: 400,
  defaultType: 'textfield',
  defaults: {
    width: 350
  },
  items: [{
    xtype:'textfield',
    width: 200,
    fieldLabel: 'Bug ID',
    name: 'bugID',
    valueField:'bugID',
    emptyText: 'ID...',
    id:'bugIDID',
    readOnly:'true'
  },{
    xtype:'textarea',
    fieldLabel: 'Description',
    name: 'description',
    valueField:'description',
    emptyText: 'Enter a new description...',
    id:'descriptionID'
  },{
    xtype:'combo',
    fieldLabel: 'Bug Type',
    name: 'Type',
    allowBlank: false,
    id:'bugTypeID',
    displayField:'label',
    store: Ext.create('extjs.store.DropType'),
    emptyText: 'Select a type...'
  },{
   xtype:'combo',
   fieldLabel: 'Bug Status',
   name: 'Status',
   allowBlank: false,
   id:'bugStatusID',
   typeAhead: true,
   displayField:'label',
   store: Ext.create('extjs.store.DropStatus'),
   queryMode: 'local',
   emptyText: 'Select a status...'

 },{
   xtype:'combo',
   fieldLabel: 'Bug Priority',
   name: 'Priority',
   id:'bugPriorityID',
   displayField:'label',
   store: Ext.create('extjs.store.DropPriority'),
   allowBlank: false,
   emptyText: 'Select a Priority...'
 },{
   xtype:'combo',
   fieldLabel: 'Assign To User',
   name: 'AssignedTo',
   id:'assignedToID',
   allowBlank: false,
   displayField:'userName',
   valueField:'userID',
   emptyText: 'Assign Bug to user...',
   store: Ext.create('extjs.store.DropFriends'),
   queryMode: 'local',


 }],
   // Reset and Submit buttons
   buttons: [
   {
    itemId: 'updateSubmit',
    text: 'Submit',
    formBind: true,
    handler: function() {
      //alert('hi');
    },
    
  }],
});

