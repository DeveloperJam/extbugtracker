/*
Bug Panel for creating bugs
*/
Ext.define('extjs.view.bug.BugPanel', {
  extend: 'Ext.form.Panel',
  alias:'widget.bugpanel',
  require:['extjs.store.DropPriority','extjs.store.DropStatus','extjs.store.DropProject','extjs.store.DropType'],
  bodyPadding: 5,
  url: 'http://localhost:9000/BugsService.asmx/createBug',
  baseParams: {AssignedTo : 3,CreatedBy: 3},
  defaultType: 'textfield',
  defaults: {
    width: 500
  },
  items: [{
    xtype:'combo',
    fieldLabel: 'ProjectName',
    name: 'projectID',
    allowBlank: false,
    displayField:'projectName',
    valueField:'projectID',
    emptyText: 'Select the Project',
    store: Ext.create('extjs.store.DropProject'),
    queryMode: 'local'
  },{
   xtype:'textarea',
   fieldLabel: 'Bug Description',
   name: 'Description',
   allowBlank: false,
   emptyText: 'Enter a description...'
 }, {
  xtype:'combo',
  fieldLabel: 'Bug Type',
  name: 'Type',
  allowBlank: false,
  displayField:'label',
  store: Ext.create('extjs.store.DropType'),
  emptyText: 'Select a type...'
},{
 xtype:'combo',
 fieldLabel: 'Bug Status',
 name: 'Status',
 allowBlank: false,
 typeAhead: true,
 displayField:'label',
 store: Ext.create('extjs.store.DropStatus'),
 queryMode: 'local',
 emptyText: 'Select a status...'
},{
 xtype:'combo',
 fieldLabel: 'Bug Priority',
 name: 'Priority',
 displayField:'label',
 store: Ext.create('extjs.store.DropPriority'),
 allowBlank: false,
 emptyText: 'Select a Priority...'
}],
   // Reset and Submit buttons
   buttons: [
   {
    text: 'Submit',
    formBind: true,
        itemId: 'bugSubmit', //only enabled once the form is valid
      }],

    });



