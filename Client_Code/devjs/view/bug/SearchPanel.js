
/*
Search Panel --TODO
*/
Ext.define('extjs.view.bug.SearchPanel', {
	extend:'Ext.panel.Panel',
	alias:'widget.searchpanel',
	items:[{
		xtype:'textfield',
		fieldLabel: 'Search',
		emptyText: 'Search Description...',
		
	},{
		xtype:'button',
		text:'Search Bugs',
		itemId: 'searchSubmit',
	}]

});

