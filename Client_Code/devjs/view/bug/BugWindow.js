
/*
Bug Window for creating bugs
*/
Ext.define('extjs.view.bug.BugWindow', {
  extend: 'Ext.window.Window',
  alias:'widget.bugwindowwindow',  
  width:600,
  height:300,
  maximizable:false,
  layout:'fit',
  title:"Create Bug",
  items: new Ext.create('extjs.view.bug.BugPanel')
  
});

