/*
Inv window for inv Panel
*/
Ext.define('extjs.view.inv.InvWindow', {
  extend: 'Ext.window.Window',
  alias:'widget.invwindow',  
  width:600,
  height:150,
  maximizable:false,
  layout:'fit',
  title:"Invite To Project",
  items: new Ext.create('extjs.view.inv.InvPanel')
  
});

