/*
Inv Panel for Inviting Users to projects
*/
Ext.define('extjs.view.inv.InvPanel', {
  extend: 'Ext.form.Panel',
  alias:'widget.invpanel',
  require: ['extjs.store.DropFriends','extjs.store.DropProject'],
  bodyPadding: 5,
  url: 'http://localhost:9000/UserProjectService.asmx/addUserToProject',
  //baseParams: {ProjectID : '3'},
  defaultType: 'textfield',
  defaults: {
    width: 500
  },
  items: [{
    xtype:'combo',
    fieldLabel: 'ProjectName',
    name: 'projectID',
    allowBlank: false,
    displayField:'projectName',
    valueField:'projectID',
    emptyText: 'Select a Project...',
    store: Ext.create('extjs.store.DropProject'),
    queryMode: 'local'
  },{
   xtype:'combo',
   fieldLabel: 'Invite User',
   name: 'userID',
   allowBlank: false,
   displayField:'userName',
   valueField:'userID',
   emptyText: 'Select a user...',
   store: Ext.create('extjs.store.DropFriends'),
   queryMode: 'local',
 }
 ],
 buttons: [{
  text: 'Submit',
  formBind: true,
  itemId: 'invSubmit',
      }],
    });
