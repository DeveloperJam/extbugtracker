
var memberStore = new Ext.data.ArrayStore({
  autoLoad: true,  
  storeId: 'memberStore',
  require: ['extjs.model.UserView'],
       // model: 'extjs.model.BugGrid',
       proxy: new Ext.data.HttpProxy({
        type: 'ajax',
        method:'GET',
        url: 'http://localhost:9000/UserProjectService.asmx/getProjectMembers', 
        extraParams: {projectID: 3},
        headers: {'Content-Type' : 'application/json'},                              
        reader: {
          type: 'json',                                           
          root: 'd'
        },    
      }),
      fields: ['userID', 'userName'] 
       //model:'extjs.model.UserView'

     });

Ext.define('extjs.view.user.MemberGrid', {
  extend: 'Ext.grid.GridPanel',
  alias: 'widget.membergrid',
  title: 'Project Members',
  selType: 'cellmodel',
  border: true,
    //width:600,
                        store: memberStore, //Ext.data.StoreManager.lookup('myStore'),          use only if multiple ui are sharing a store 
                        columns: [
                        { header: 'UserID', dataIndex: 'userID', sortable: true , flex: 1, sortable: false }, 
                        { header: 'UserName', dataIndex: 'userName',   flex: 1, sortable: false },
                     ],
                       // renderTo: 'panel',

                     });

