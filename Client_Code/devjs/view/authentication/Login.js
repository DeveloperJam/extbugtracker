
var login = Ext.define('extjs.view.authentication.Login', {
    extend:'Ext.form.Panel',
    alias:'widget.login',
    title: 'Login',
    bodyPadding: 5,
    width: 350,
    layout: 'anchor',
    // The form will submit an AJAX request to this URL when submitted
    url: 'http://localhost:9000/AuthService.asmx/loginUser',
    dataType: "json",
    // Fields will be arranged vertically, stretched to full width
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    defaultType: 'textfield',
    items: [{
        fieldLabel: 'UserName',
        name: 'UserName',
        allowBlank: false,
        itemId: 'txtuser'
    },{
        fieldLabel: 'Password',
        name: 'Password',
        inputType: 'password',
        allowBlank: false,
        itemId: 'txtpass'
    },{
            xtype:'checkbox',
            fieldLabel: 'Remember me',
            name: 'remember'
       }],

    // Reset and Submit buttons
    buttons: [ {
        text: 'Submit',
        formBind: true, //only enabled once the form is valid
        disabled: true,
        itemId: 'btnLogin'
      }
  ]
});
