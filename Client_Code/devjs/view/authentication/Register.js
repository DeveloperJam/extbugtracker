 Ext.define('extjs.view.authentication.Register', {
    extend:'Ext.form.Panel',
    alias:'widget.register',
    title: 'Register',
    bodyPadding: 5,
    width: 350,
    layout: 'border',
    // The form will submit an AJAX request to this URL when submitted
    url: 'http://localhost:9000/AuthService.asmx/createUser',
    // Fields will be arranged vertically, stretched to full width
    layout: 'anchor',
    defaults: {
        anchor: '100%'
    },
    // The fields
    defaultType: 'textfield',
    items: [{
        fieldLabel: 'UserName',
        name: 'UserName',
        allowBlank: false
    },{
        fieldLabel: 'Password',
        name: 'Password',
        inputType: 'password',
        allowBlank: false
    }],
    // Reset and Submit buttons
    buttons: [{
        itemId: 'regSubmit', 
        text: 'Submit',
        formBind: true, //only enabled once the form is valid
        disabled: true,
    }]
});