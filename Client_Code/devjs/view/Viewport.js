/*
Main Viewport for workspace page, views are placed inside this viewport
*/
Ext.define('extjs.view.Viewport', {
  extend: 'Ext.container.Viewport',
  alias:'widget.viewport',
  layout: 'border',
  requires:['extjs.view.authentication.Login','extjs.view.authentication.Register'],
   items: [{
     region: 'north',
     html: ' <div id ="mainheader"></div>',
     border: false
   },{
     region: 'center',
     activeTab: 0,
     type: 'vbox',
     items: [{
       xtype: 'login',
     },{
      xtype: 'register',
    }

    ]                      
}]
});
