

Ext.define('extjs.view.Workspace', {
	extend: 'Ext.container.Viewport',
	alias:'widget.workspace',
	layout: 'border',
	requires:['extjs.view.bug.BugGridPanel','extjs.view.CreationPanel','extjs.view.project.ProjectCombo','extjs.view.bug.SearchPanel','extjs.view.user.MemberGrid'],
	items: [{
		region: 'north',
		html: ' <div id ="mainheader"></div>',
		border: false
	},{
		region: 'south',
		html: '<div id ="mainfooter"></div>',
		height: 100,
		minHeight: 100
	},{
		region: 'center',
		activeTab: 0,
		type: 'vbox',
		html: '<div id ="panel"></div>',
		items: [{
			xtype: 'projectcombo',
		} ,{
			xtype: 'buggridpanel',
		}      
		]           
	},{
		region: 'east',
		title: 'Issue Toolbox',
		html: '<div id ="bugtool"></div>',
		width: 250,
		items: [{xtype: 'createbug',}]
	},
	{
		region: 'west',
		title: 'Sorting Toolbox',
		//html: '<div>Show Only Satus: In progress  Closed  Cannot Replicate   Needs Development</div><div>Show Only Priority: Critical Closed  Major   Minor Trivial</div>',
		width: 250,
		items: [{xtype: 'searchpanel',},
		{xtype: 'membergrid',}],
		
	}]
});