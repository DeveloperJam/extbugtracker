var dropFriends = new Ext.define("extjs.store.DropFriends",{
    extend: 'Ext.data.Store',
    autoLoad: true,  
    storeId: 'dropFriends',
    proxy: new Ext.data.HttpProxy({
       type: 'ajax',
     //  extraParams: {UserID : 3},
       url: 'http://localhost:9000/AuthService.asmx/getUserFriends', 
       headers: {'Content-Type' : 'application/json'},                              
       reader: {
        type: 'json',                                           
        root: 'd'
  },   
}),
      model:'extjs.model.User'
 });


