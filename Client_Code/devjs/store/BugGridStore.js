
/*
Unused
*/
var BugGridStore =  new Ext.define('extjs.store.BugGridStore')
extend : 'Ext.data.ArrayStore',
autoLoad: true,  
storeId: 'bugGridStore',
       proxy: new Ext.data.HttpProxy({
        type: 'ajax',
        method:'GET',
        url: 'http://localhost:9000/BugsService.asmx/getProjectBugs', 
        extraParams: {projectID: [1]},
        headers: {'Content-Type' : 'application/json'},                              
        reader: {
          type: 'json',                                           
          root: 'd'
        },    
      }),
       model:'extjs.model.BugGrid'
});

