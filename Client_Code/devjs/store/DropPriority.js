var dropPriority = new Ext.define("extjs.store.DropPriority",{
	extend: 'Ext.data.Store',
	storeId: 'dropPriority',
	fields : ['key' ,'label'],
	data : [
	{key:'CRT', label: 'Critical'},
	{key:'MAJ', label: 'Major'},
	{key:'MIN', label: 'Minor'},
	{key:'TRI', label: 'Trivial'}
	
	]
});




