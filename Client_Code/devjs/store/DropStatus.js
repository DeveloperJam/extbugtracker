var dropStatus = new Ext.define("extjs.store.DropStatus",{
	extend: 'Ext.data.Store',
	storeId: 'dropStatus',
	fields : ['key' ,'label'],
	data : [
	{key:'INP', label: 'In progress'},
	{key:'CLO', label: 'Closed'},
	{key:'CAN', label: 'Cannot Replicate'},
	{key:'NDE', label: 'Needs Development'}
	
	]
});
