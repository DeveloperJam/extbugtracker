var dropProject = new Ext.define('extjs.store.DropProject',{
  extend: 'Ext.data.Store',
  autoLoad: true,  
  storeId: 'dropProject',
  proxy: new Ext.data.HttpProxy({
   type: 'ajax',
   extraParams: {UserID : 3},
   url: 'http://localhost:9000/UserProjectService.asmx/getUserProjects', 
   headers: {'Content-Type' : 'application/json'},                              
   reader: {
    type: 'json',                                           
    root: 'd'
  },   
}),
  model:'extjs.model.ProjectCombo'
});
