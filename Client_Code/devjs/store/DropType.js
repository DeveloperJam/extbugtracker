var dropStatus = new Ext.define("extjs.store.DropType",{
	extend: 'Ext.data.Store',
	storeId: 'dropType',
	fields : ['key' ,'label'],
	data : [
	{key:'NEW', label: 'New Feature'},
	{key:'BFI', label: 'Bug Fix'},
	{key:'TES', label: 'Testing'},
	{key:'Imp', label: 'Improvement'}
	
	]
});
